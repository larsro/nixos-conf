# Common settings for desktop environments

{ config, pkgs, ... }:

{
  imports =
    [ # Include-files
    ];

  fonts = {
    fonts = with pkgs; [
      nerdfonts
    ];
    fontconfig = {
      penultimate.enable = true;
      defaultFonts = {
        monospace = [ "Cousine" ];
        sansSerif = [ "Arimo" ];
        serif = [ "Tinos" ];
      };
    };
  };

  environment.systemPackages = with pkgs; [
    makemkv
  ];

  # Enable CUPS to print documents.
  services.printing.enable = true;
}
