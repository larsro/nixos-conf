# Configuration for the Sway desktop environment

{ config, pkgs, ... }:

{
  imports =
    [ # Common settings for desktop environments
      ./common.nix
    ];

  networking.connman.enable = true; # Enable ConnMan network manager

  programs = {
    # Enable Sway
    sway.enable = true;
    sway.extraPackages = with pkgs; [ 
      i3lock
      i3blocks
      rofi
      xwayland
      mpv
      neomutt
      newsboat
      qutebrowser
      ranger
      rtv
      termite
      weechat
    ];
    sway.extraSessionCommands = "
      export XKB_DEFAULT_LAYOUT=no
      export XKB_DEFAULT_VARIANT=nodeadkeys
      # Change the Keyboard repeat delay and rate
      export WLC_REPEAT_DELAY=660
      export WLC_REPEAT_RATE=25
    ";
  };

  services = {
    acpid = {
      lidEventCommands = ''
if grep -q closed /proc/acpi/button/lid/LID/state; then
  _user="$(id -u -n)"
  date >> /tmp/i3lock.log
  DISPLAY=":0.0" XAUTHORITY=/home/$"{_user}"/.Xauthority ${pkgs.i3lock}/bin/i3lock &>> /tmp/i3lock.log
fi
'';
    };
  };
}
