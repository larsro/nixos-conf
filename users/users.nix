# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.lasse = {
    createHome = true;
    home = "/home/lasse";
    group = "users";
    extraGroups = ["wheel" "video" "audio" "disk" "docker" "sway"];
    isNormalUser = true;
    uid = 1000;
    shell = pkgs.zsh;
    openssh.authorizedKeys.keys = [''
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCvcOlzbIJ7d1olpQsf2A+iow3YPIR/sm5GxD+GPIS2ziKXbvD7o4MSVK79O18I5oEBInPvM2SpgNqYOGFplcLvNFj85ncYas6hU9l+1D0BqFkMCgJGqke1kZxmU0CzK1y7xl8wXcH3wl4x
9c+HXbDP7PLn+gkNh50WC9JtZirHWIeaW21n80O/qP69+uvjBbXlBdSLzYO9qpl2ZBo4QYPC9/TTtpeOoEOdhWiVO+91Jwj2qP7Q0l3mHK1os/Z89wqaUCmF9NeqGIIVuH5EvqB2Ox0/MrMJiGgzP0RsGwOKz2+ttxs3gGgLq6BDX4QB+/vH
SPlJJGqXmVfqO++TloTz lasse
    ''];
  };
}
