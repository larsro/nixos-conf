# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  # Use the GRUB 2 boot loader.
  #boot.loader.grub.enable = true;
  #boot.loader.grub.version = 2;

  # Select internationalisation properties.
  i18n = {
    consoleFont = "Lat1-Terminus16";
    consoleKeyMap = "no-latin1";
    defaultLocale = "en_US.UTF-8";
    #consoleColors = [ "2e3440" "3b4252" "434c5e" "4c566a" "d8dee9" "e5e9f0" "eceff4" "8fbcbb" "88c0d0" "81a1c1" "5e81ac" "bf616a" "d08770" "ebcb8b" "a3be8c" "b48ead" ];
  };

  # Set your time zone.
  time.timeZone = "Europe/Oslo";

  nixpkgs.config =
  {
    packageOverrides = pkgs: rec
    {
#      acpilight = pkgs.callPackage ../pkgs/acpilight.nix { };
    };
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    brightnessctl
    git
    htop
    neovim
    nix-prefetch-scripts
    powertop
    stow
    wget
    which
  ];

  # List services that you want to enable:

  # zsh
  programs.zsh = {
    enable = true;
    autosuggestions.enable = true;
    ohMyZsh.enable = true;
    ohMyZsh.plugins = [ "git" ];
    ohMyZsh.theme = "frisk";
    syntaxHighlighting.enable = true;
  };

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = false;

  virtualisation.docker.enable = true;
}
