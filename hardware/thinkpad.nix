{ config, pkgs, ... }:

{
  hardware.cpu.intel.updateMicrocode = true;
  hardware.brightnessctl.enable = true;
  # Enable sound.
  sound = {
    enable = true;
    mediaKeys.enable = true;
    mediaKeys.volumeStep = "1%";
    extraConfig = ''
      defaults.ctl.card 1
      defaults.pcm.card 1
      defaults.timer.card 1    
    '';
  };
  hardware.pulseaudio.enable = true;
  environment.systemPackages = with pkgs; [
    linuxPackages.tp_smapi
    linuxPackages.acpi_call
  ];
  boot = {
    kernelParams = [
      # Kernel GPU Savings Options (NOTE i915 chipset only)
      "acpi_osi=Linux"
      "acpi_backlight=vendor"
      "i915.enable_rc6=7"
      "i915.enable_fbc=1"
      "i915.enable_dc=2"
    ];
    blacklistedKernelModules = [
      # Kernel GPU Savings Options (NOTE i915 chipset only)
      "sierra_net" "cdc_mbim" "cdc_ncm"
    ];
  };
  services = {
    acpid = {
      enable = true;
    };
    tlp = {
      enable = true;
    };
  };
  boot.extraModprobeConfig = ''
  options snd_hda_intel power_save=1
  '';
  systemd.services.tune-powermanagement = {
    description = "Tune Powermanagement";
    serviceConfig.Type = "oneshot";
    serviceConfig.RemainAfterExit = true;
    wantedBy = [ "multi-user.target" ];
    unitConfig.RequiresMountsFor = "/sys";
    script = ''
      echo '1500' > '/proc/sys/vm/dirty_writeback_centisecs'
    '';
  };
}
