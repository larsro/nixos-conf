{ stdenv, fetchurl, python36 }:
stdenv.mkDerivation rec {
  name = "acpilight-1.1";

  src = fetchurl {
    url = https://gitlab.com/wavexx/acpilight/-/archive/v1.1/acpilight-v1.1.tar.bz2;
    sha256 = "0a5ef16a39c05c1eb195823d906c08f8f912aa48122b31e6dd364c7f9d6c37aa";
  };

  #unpackPhase = "true";
  dontBuild = "true";

  pyenv = python36.withPackages (pythonPackages: with pythonPackages; [
    argparse
  ]);

  buildInputs = [ pyenv ];

  installPhase = ''
    pwd
    ls -l
    mkdir -p $out/etc/udev/rules.d
    cp 90-backlight.rules $out/etc/udev/rules.d/90-backlight.rules
    mkdir -p $out/bin
    cp xbacklight $out/bin/xbacklight
    gzip xbacklight.1
    mkdir -p $out/share/man/man1
    cp xbacklight.1.gz $out/share/man/man1/xbacklight.1.gz
  '';

  meta = {
    homepage = "https://gitlab.com/wavexx/acpilight";
    description = "ACPI backlight control";
    license = stdenv.lib.licenses.gpl3;
  };
}
